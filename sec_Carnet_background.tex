\begin{table*}[t] 
	\centering
	\caption{Functionalities and applications provided in {\CARNET{}}.}
	\label{table:apps} 
	\begin{tabular}{|p{0.18\columnwidth}| p{0.45\columnwidth}| p{1.3\columnwidth}|} 
		\hline
		\textbf{Type} & \textbf{Name} & \textbf{Note} \\ \hline \hline
		\multirow{4}{0.18\columnwidth}{\textbf{Data quality}} &  Outlier Detection   &    Detect outlier data from vehicle-data streams. \\ \cline{2-3} 
		&  Data Cleaning &   Filter outlier data or repeated data.  \\ \cline{2-3} 
		&Data Quality Inspection  &  Analyze data quality from multiple dimensions and generate reports periodically. \\ \cline{2-3} 
		&  Data Filling &  Fill the missing data or replace the wrong data in the streams with various techniques.  \\ \hline
		\multirow{7}{0.18\columnwidth}{\textbf{Fleet Management}} & Electronic Fence  &  Based on user-defined virtual fence (on map) and vehicle location, detect the activities of fence entering and leaving. \\ \cline{2-3} 
		&  Vehicle Tracking  & Track vehicle status including location, speed, engine parameter, error, and warning in real-time. Provide fast feedback to drivers and the  control department. \\ \cline{2-3} 
		&  Fleet Distribution  &  Fast index and visualize the dynamic fleet distribution in the space dimension; support the vehicle scheduling.  \\ \cline{2-3} 
		&  Gasoline Anomaly Detection  &   Detect abnormal gas consumption in case of stealing or leaking.  \\ \cline{2-3} 
		& Fleet Operation Report  & Produce statistics of the driving data; offer a regular report on the running status of the fleet, such as daily mileage, gas consumption, valid mileage. \\ \cline{2-3} 
		&  Driving Behavior Analysis  &  Evaluate driving skills in terms of safe and skillful driving. \\ \cline{2-3} 
		& Driving Event Detection  & Detect driving events, such as sudden braking, from data stream.  \\ \hline
		\multirow{3}{0.18\columnwidth}{\textbf{Decision Making}} &  Driver Profit Assessment  & Calculate the profitable mileage and gasoline consumption of a certain trip by using historical data. \\ \cline{2-3} 
		& Order Prediction  &  Based on historical order data, predict the order distribution in the future so that cars can be dispatched in advance.\\ \cline{2-3} 
		& Dynamic Pricing  &  Design a flexible price model based on the real-time passenger demand to achieve maximum profit,  e.g., increasing the price for peak hour. \\ \hline
		\multirow{3}{0.18\columnwidth}{\textbf{Other}} &  System Monitoring & Provide a comprehensive view for the system-health status. \\ \cline{2-3} 
		&  Multi Stream Fusion&  Merge multiple streams such as the driving events and the GPS locations to extract correlated patterns.   \\ \cline{2-3} 
		&  Trajectory Compression  &  Losslessly compress huge historical trajectory data.\\ \hline
	\end{tabular}
	\vspace{-1.2em}
	\label{table:metadata}
\end{table*}


\section{Background of CarStream}
\label{sec:background}

The concept of IoV is originated from IoT. There can be multiple definitions of IoV. For example, traditional IoV defines a vehicular network connected by vehicles through the Radio Frequency Identification (RFID) technology. A technology known as Vehicle to Vehicle (V2V) can further share traffic and vehicle information through running vehicles, providing a local-area solution for transportation safety and efficiency. With the wide adoption of mobile Internet, traditional IoV has evolved to a wide-area network deployment that combines in-vehicle network and inter-vehicle network. In such IoV, the vehicle sensors are connected to the Electronic Control Unit (ECU) through CAN-BUS, and a cloud-centric vehicular network is constructed with vehicles connected to the backend servers through the mobile network. The backend servers act as a central intelligent unit that collects, analyzes, stores the data uploaded by vehicles, and further send the value-added information back to vehicles.

By successfully fulfilling many requirements in the transportation domain, IoV has been for years one of the most popular applications in the IoT paradigm. But few large IoV deployments exist, due to the high deployment and maintenance cost for individually owned cars. Private car services, such as Uber \cite{Uber} and Lyft \cite{Lyft}, are successful Internet-driven industrial examples. The underlying system supporting such services can be regarded as a simplified version of IoV where smartphones, instead of dedicated sensors, are used to connect passengers, drivers, and vehicles. This technology has given rise to a huge market targeted by many similar companies established in recent years. To implement an IoV platform, the basic functional requirements should at least include vehicle-data collection, storage, analysis, and services. Vehicles can upload driving data, including instant locations and vehicle-engine statuses, to the backend servers. The data can be used in both real-time and offline vehicle-management applications.

In this paper, we discuss the design and implementation issues of a cloud-centric IoV system named \CARNET{}. \CARNET{} is based on a real-world chauffeured car service company in China, named UCAR. Such service is similar to the Uber private-car service where the drivers take orders from passengers through mobile phones and drive them to their destinations. But the UCAR service also has several unique characteristics: the fleet belongs to the company, and drivers are employees of the company. This centralized business model provides a good opportunity for deploying an Onboard Diagnostic (OBD) connector, a data-sampling device for vehicles. The OBD is connected to the in-vehicle Controller Area Network bus (CAN-BUS). Under a defined interval, the OBD collects vehicle parameter values such as speed, engine RPM, and vehicle error code. The collected data are transmitted back to the backend server through an integrated wireless-communication module. With the help of such near real-time vehicle data, we can monitor the driving behavior in a fine granularity and react in a timely manner to ensure the safety and quality of the trip services. The data also provide a direct view of vehicle status, being valuable in real-time fleet scheduling and long-time operational management. 

Besides the OBD connector, each driver also uses a mobile phone to conduct business-related operations such as taking orders, navigation, changing service status. Therefore, the mobile phone generates a separate data stream, namely business data, and uploads the data to the server in a different channel.


\CARNET{} is designed to process those vehicle data and business data as a back-end service. In general, \CARNET{} collects, stores, and analyzes the uploaded data in terms of vehicle and driver management. So far, a major part of the fleet, over 30,000 vehicles, are connected to \CARNET{}. Those vehicles are distributed among 60 different cities in China. Each vehicle uploads a data packet to the server every 1 to 3 seconds when the vehicle is running. Thus, the backend system needs to process nearly 1 billion data instances per day. The scale of the fleet had expanded from the initial 1500 to 30,000 within a few months; this business expansion poses a horizontal scalability request to the underlying platform.

Figure \ref{fig:data} illustrates the multiple data that are collected and processed in \CARNET{} during a typical day. After running for 3 years, totally 40 TB vehicle data are collected by \CARNET{}. The data can be roughly classified into four types: vehicle status, passenger order, driver activity, and trajectory data, as shown in Figure 1. The data in \CARNET{} are not only large in volume but also comprehensive.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.95\linewidth]{picture/data.pdf}
	\vspace{0em}
	\caption{The data processed in \CARNET{}.}
	\vspace{-2em}
	\label{fig:data}
\end{figure}

However, underneath the abundant data are a series of data-quality problems that need to be addressed. For example, a full set of the vehicle-data parameters contain more than 70 items, but because the vehicles are designed by multiple manufacturers, different subsets of the whole parameter set may be collected from different vehicles. Some vehicles have gear parameter data while others do not. Meanwhile, the running of the vehicle always causes vibration, which in turn affects the correctness of sensor readings such as the remaining gasoline level. Other data-quality problems that \CARNET{} faces include disorder and delay, which may be caused by multiple factors such as distributed processing and network problems. Per the statistics of our dataset, nearly 10\% data are disordered or delayed. Such problems may have a significant influence on different aspects of the system, especially on the processing accuracy. 

A special characteristic of the data stream in \CARNET{} is that the data stream is subjected to the traffic pattern: the data amount uploaded in peak hours can be as much as 80 times larger than that are in off-peak hours. For this reason, \CARNET{} also needs to deal with burst streams. Figure \ref{fig:datadynamic} illustrates the data amount of 24 hours in three days, and clearly shows a traffic-like pattern with morning and evening peak hours. 

\begin{figure}[t]
	\centering
	\includegraphics[width=0.95\linewidth]{picture/datadynamic.pdf}
	\vspace{-0.5em}
	\caption{The data amount changes with time.}
	\vspace{-1em}
	\label{fig:datadynamic}
\end{figure}

Based on the data, \CARNET{} provides multiple functionalities and applications to facilitate the fleet management for chauffeured car service. As shown in Table \ref{table:apps}, the applications have a dependency on each other. \emph{Data filling}, for example, relies on data cleaning, which in turn has a dependency on outlier detection. The application of fleet operation report has a strong dependency on the functionalities of data cleaning and outlier detection because most of the gasoline data are very inaccurate due to the influence of vehicle vibration. \emph{Multi stream fusion} is another application that other applications depend on. Because the data are collected from multiple sources with different frequencies, the fusion (or matching) becomes a common requirement. For example, an interesting application in \CARNET{} is to detect abnormal gasoline consumptions. In this application, we need to merge the data stream of driver activities with the data stream of vehicle statuses so that we can learn whether the gasoline consumption is reasonable. Sudden drops of the level of remaining gasoline are detected by the system, but a further analysis of the data suggests that most of these phenomena are caused by the influence of parking position. However, such phenomena can also be caused by gasoline stealing, which was not expected by fleet managers. 

The applications in \CARNET{} also differ on the performance requirements, as we can infer from Table \ref{table:apps}. Some applications may have high-performance requirements as they are time-critical or safety-critical missions. The various application requirements increase the complexity in designing an integrated system that fits all of the listed applications. 


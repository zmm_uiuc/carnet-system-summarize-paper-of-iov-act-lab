 

\begin{table*}[t]
	\centering
	\caption{Common data-quality issues in IoV scenarios.}
	\label{table:dataquality}
	%\begin{tabular}{|l|l|l|l|l|}
	\begin{tabular}{|p{0.16\columnwidth}| p{0.25\columnwidth}| p{0.3\columnwidth}| p{0.4\columnwidth}| p{0.65\columnwidth}|} 
		\hline
		\textbf{Category} & \textbf{Problem}  & \textbf{Consequence}  & \textbf{Cause} & \textbf{Solution} \\ \hline\hline
		
		\multirow{3}{0.16\columnwidth}{\textbf{Lack of data}} & \multirow{2}{0.25\columnwidth}{Data Loss} & \multirow{2}{0.3\columnwidth}{No result/ Inaccurate result} & Network failure & \multirow{2}{0.65\columnwidth}{Redundant deployment. Interpolation by data patterns.} \\ \cline{4-4}
		
		&                         &                                              & Software fault  &                                                                                   \\ \cline{2-5} 
		& \rule{0pt}{0.3cm}Insufficient Data     &     Inaccurate result   &          Physical limitation (vehicle limitation)         &   Interpolation by data patterns.                  \\ \hline
		\multirow{6}{0.16\columnwidth}{\textbf{Wrong data}} & \multirow{3}{0.25\columnwidth}{Disorder} & \multirow{3}{0.3\columnwidth}{Wrong result/ Inaccurate result} & Distributed nature of processing platform & \multirow{3}{0.65\columnwidth}{Delay and wait. Dropping out-of-date data. Order guarantee design (in app layer). Fixing with prediction.} \\ \cline{4-4}
		&                   &                   & Network transition &                   \\ \cline{4-4}
		&                   &                   & Store and forward design of data nodes &                   \\ \cline{2-5} 
		& \multirow{3}{0.25\columnwidth}{ Wrong/Outlier Data} & \multirow{3}{0.3\columnwidth}{Wrong result} & Hardware malfunction & \multirow{3}{0.65\columnwidth}{Outlier detection. Data cleaning. Fixing with data patterns.} \\ \cline{4-4}
		&                   &                   & Inaccurate sensors &                   \\ \cline{4-4}
		&                   &                   & Other physical reasons &                   \\ \hline
	\end{tabular}
	\vspace{0em} 
\end{table*}



\section{Lessons Learned}
\label{sec:experience}


\textbf{Lesson Learned 1: Application-level monitoring is necessary for achieving high reliability.}

Infrastructure monitoring and computing-platform monitoring are the most commonly adopted techniques for assuring system reliability. Doing so can allow us to take timely reactions when a problem occurs. We can also use the monitoring information to help to utilize system resources by adjusting the deployment. However, we learn through the maintaining of \CARNET{} that infrastructure-level and platform-level monitoring are insufficient to provide highly dependable services, especially in safety-critical scenarios. Our insight is that, it is necessary to monitor the application-level because application monitoring provides direct information to answer key questions such as \emph{which processing step is the bottleneck?} or \emph{what caused the cutoff of the data stream?} By monitoring the key parameters in an application, such as local buffer length, processing delay, and processing rate, we can have a deeper understanding of the application's runtime behavior. These parameters can be collected and analyzed on the fly, helping operators to take actions for service failure prevention or recovery.

\textbf{Lesson Learned 2: Low data quality widely exists in IoV. A fixing model extracted from historical data patterns can be helpful.}

According to our experience, IoV applications usually face a severe issue of low data quality, such as data loss, data disorder, data delay, insufficient data, and wrong data, etc. There are multiple causes of those problems. For example, the data disorder can be attributed to the distributed processing mode of the system. Because the data are processed concurrently so that later data are possible to be processed earlier than the earlier-arrived data, resulting in a disordered sequence. The disorder can also be caused by the parallel transmission of the network or the defect of the processing in the application layer. Similarly, the problem of wrong/outlier data can be attributed to multiple factors such as hardware malfunction, the sensor jitter, or other physical reasons (e.g., vehicle vibration). In IoV scenarios, these problems usually need to be addressed in real time. A general list of the problems and the corresponding causes are summarized in Table \ref{table:dataquality}.

There can be multiple solutions to these problems. Let us take dealing with data disorder as an example. A basic solution is delay-and-wait: the processing is pended until all the data instances within the sliding window are ready. This solution compromises real-time performance to achieve the accuracy. Another solution is providing sequence guarantee in the application when designing the system. For example, the data of each vehicle are processed by the same processing node. Hence, the processing is paralleled for the fleet but is sequential for any specific vehicle. This solution also has its own limitation: it sacrifices the flexibility and scalability of the processing subsystem. 

In \CARNET{}, we employ a data-driven solution to address the data-quality problem. In particular, we make use of the patterns in the data and drive a fixing model to represent the data patterns, and then we use the fixing model online to improve the data quality. Our empirical study on the data reveals clear patterns from the data; such data nature is largely due to the regularity in driving. For example, the trajectory generated along a road follows the trend of that road, and drivers would maintain the vehicle in a stable status when driving on a highway. During the acceleration stage, most drivers would take a steady acceleration. The model extraction takes some time but the online running can be fast and efficiency.  Note that such solution is only applicable to the problem of local data quality where a small part of the stream is missing or disordered. 

\textbf{Lesson Learned 3: In large-scale scenarios, the linked-queue-based sliding-window processing may cause a performance issue.}

In stream-processing applications, the sliding window is one of the most commonly used functionalities. Typically, the sliding window is implemented with a linked queue. A new data is appended to the queue when the data comes. Meanwhile, when the queue has reached the maximum length, the oldest data will be removed. The enqueue and dequeue operations have high performance when the dataset is relatively small. However, when it comes to large-scale data streams, such linked-queue-based solution may suffer from poor performance due to the frequent memory-management (allocate and free) operations. 

To improve the performance, we suggest using a Round-Robin Queue (RRQ) to implement the sliding window. The RRQ reuses memory to improve the performance of processing. Compared with the linked-queue-based solution, the runtime of processing the same quantity of data with RRQ can be reduced by 80\%.  

\textbf{Lesson Learned 4: A single storage is usually insufficient; a heterogeneous storage architecture is necessary for managing large-scale vehicle data.}

As a typical big data processing scenario, IoV needs to manage a huge quantity of vehicle data. In this scenario, data management faces severe challenges: the data volume is huge, and the applications are of variety such that different data-access requirements need to be satisfied. For example, decision-making-related applications require a large throughput of the data access, while time-critical applications require higher data-access performance. Based on our experience, there might not be a one-for-all storage platform that satisfies the requirements of all the applications, and a heterogeneous storage architecture is usually necessary for maximizing the system performance. 

We summarize two tips on how to improve the database performance from an architecture perspective.
\begin{itemize}
	\item Separate hot data with archived (cold) data; use in-memory caching as the exchange media for the hot data.
	\item Avoid scanning a big dataset by extracting a small dataset with preprocessing.
\end{itemize}

\textbf{Lesson Learned 5: Single platform may not be sufficient for multiple requirements in IoV.}

Simplicity is an important principle in system design: the benefit of a simple design includes ease of maintenance, upgrade, and use. For general computing system, high performance often comes with simplicity of design. However, for IoV system, which has multiple complex application requirements, simple design may not be sufficient to fulfill the requirements of each application. It's also hard for a monolithic system design to be flexible enough to keep up with the changing of service requirements. In \CARNET{}, we adopt a multi-platform design for the IoV applications. We carefully evaluate the requirement of each application and choose the best platform to deploy the application. Designing in such manner indeed increases the system complexity and redundancy, but by using a robust data bus, processing of multiply data flows can be loosely coupled and fulfill the requirements of IoV together. 
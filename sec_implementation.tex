
\section{Implementation Paradigm}
\label{sec:implemenation} 
The overall structure of \CARNET{} and the connections between different modules are illustrated in Figure \ref{fig:modules}.
In this section, we discuss the detailed implementation of \CARNET{} from three main parts: the processing subsystem, streaming data management subsystem, and the three-layered monitoring subsystem. We show how to combine different technologies to accomplish a high-performance data-analytics system for IoV. For each part, we present the problems in the implementation along with the insights of solving these problems.

The requirements of applications in \CARNET{} are summarized in Table \ref{table:requirement}.

\begin{table}[t]
	\centering
	\caption{Application requirements of \CARNET{}.}
	\label{table:requirement}
	\begin{tabular}{|p{0.25\columnwidth}|p{0.71\columnwidth}|}
		\hline
		\textbf{Requirement} & \textbf{Detail} \\ \hline \hline
		\textbf{Scalability} &With the increase of the fleet scale, we need to be able to improve the ability of the processing platform. We also need to be able to add additional capacity to our data-storage subsystem.  \\ \hline
		\textbf{Low Latency} & After data are uploaded by a vehicle, they must be processed immediately. The processed result should be provided with low latency. \\ \hline
		\textbf{High Throughput} & On one hand, the system needs to be able to receive and store all the uploaded data. On the other hand, the system needs to be able to provide a fast query for multiple users. \\ \hline
		\textbf{High Reliability} & \CARNET{} needs to provide a highly reliable data-analytics system to users, including drivers, fleet managers, and passengers, to ensure the dependability of the services. \\ \hline
		\textbf{High Availability} & \CARNET{} needs to provide sustainable services that are robust enough to tolerate various unstable environmental conditions. \\ \hline
	\end{tabular}
\end{table}

\subsection{Processing Subsystem}

As introduced in the preceding sections, the data processed in \CARNET{} are uploaded by vehicles. With the continuous vehicle running, the data stream formed by data packets comes continuously. Thousands of small streams are merged into a big stream in the cloud. Such data are naturally distributed; therefore, a natural solution to process the data is using a distributed platform for stream processing. There can be multiple technological solutions for data-stream processing, such as Storm~\cite{Storm}, JStorm, Flink~\cite{Flink}, Spark Streaming~\cite{Zaharia, Spark}, Flume~\cite{Flume}, Samza~\cite{Samza}, and S4~\cite{Neumeyer}. 

Apache Storm \cite{Storm} is a distributed real-time computating platform for processing a large volume of high-velocity data. Based on the master-worker paradigm, Storm uses topology to construct its computing tasks. Storm is a fast, scalable, fault-tolerant, and reliable platform; it is easy to set up and operate. JStorm is a Java version of Storm while Storm is implemented with Clojure, making it inconvenient for big-data developers who are mostly familiar with Java or C++. Compared with Storm, JStorm is more stable and powerful. Apache Flink~\cite{Flink} is an open-source stream processing framework for distributed, high-performance, always-available, and accurate data streaming applications. Spark Streaming~\cite{Zaharia,Spark} is an extension of the core Spark API; such extension enables scalable, high-throughput, fault-tolerant stream processing of live data streams. In Spark Streaming, data can be processed using complex algorithms expressed with high-level functionalities. Apache Flume~\cite{Flume} is a distributed, dependable, and available service for efficiently collecting, aggregating, and moving a large volume of log data or streaming event data. Targeting at processing log-like data, Flume also provides a simple extensible data model for online analytics applications. Apache Samza~\cite{Samza} is a relatively new framework for distributed stream processing. It uses Apache Kafka for messaging, and Apache Hadoop YARN to provide fault tolerance, processor isolation, security, and resource management. S4~\cite{Neumeyer} is a general-purpose, distributed, scalable, fault-tolerant, pluggable platform that allows programmers to easily develop applications for processing continuous unbounded streams of data. There are also some other stream processing platforms such as Amazon Kinesis, which runs on Amazon Web Service (AWS), and IBM InfoSphere Streams. 

The preceding platforms share some common characteristics in that they are all scalable and high performance, and can process millions of data instances per second. They all provide basic reliability guarantee and support multiple programming languages. It is not easy to generally assess the pros and cons of each system. However, for the given requirements, if the decision has been narrowed down to choosing between those platforms, the choice usually depends on the following considerations:

\begin{itemize}
	\item \textbf{System Cost}. Cost is an important factor in selecting a technology since small enterprises or research groups might not be able to afford to pay for services that are not free.
	\item \textbf{Development Cost}. Development cost includes the human cost of deploying the infrastructure platforms and developing the applications. It would be easier to develop a comprehensive data-analytics system if the selected platforms have good support to be integrated together. Meanwhile, the deployment could also be part of the human cost as sometimes it is easy to sink days or weeks of the development time into making open-source platforms to be a scale-ready, production environment.
	\item \textbf{Upgrade Cost}. Business requirements change over time, and the corresponding applications also need to change accordingly. In an earlier stage of a production system, such evolution is especially frequent. Developers often suffer from such circumstance; however, the problem can hardly be attributed to the processing system if the platforms support an easy upgrade of the applications in a way that developers can easily reuse the existing work, so that the upgrade can be less painful.
	\item \textbf{Language Paradigm}. This consideration includes two aspects. The first is what language developers want to use to develop their applications. The second is what language the infrastructure is developed with. Developers prefer to choose the language that they are familiar with to develop their applications. Once they want to improve the infrastructure platforms, it would be more convenient if the platform is developed with the language that developers are familiar with.
	\item \textbf{Processing Model}. Different platforms support different processing models. For example, Storm uses topology to organize its steps of processing the data streams. The processing model decides which platform fits best in implementing the applications. 
\end{itemize}

After a considerable investigation and experimental exploration, we adopt JStorm as the basic stream-processing platform in \CARNET{}. The main reason is that, besides the basic high-performance, distributed, fault-tolerant characteristics of stream processing, JStorm also uses topology as the computing task model; this characteristic is especially suitable for implementing the aforementioned IoV applications. As discussed earlier, most IoV applications either have a dependency on each other or share similar functionalities. An example of such dependency is shown in Figure \ref{fig:topo}. It is common to develop and deploy an independent computing task for each application. But sometimes we might need to merge different tasks to improve resource utilization and development efficiency. With topology as the programming model, developers can integrate multiple applications by merging similar processing tasks or split a huge task into smaller (and thus more reusable) function modules. Smaza provides another solution by using KAFKA, a distributed messaging platform, as a data hub. Each task outputs its processing results to KAFKA, and such results can be further used by other tasks. 

\begin{figure}[t]
	\centering
	\includegraphics[width=0.95\linewidth]{picture/topology.pdf}
	\vspace{-1em}
	\caption{An example of dependency among applications.}
	\vspace{-1em}
	\label{fig:topo}
\end{figure}

Another reason of using JStorm is that both its implementation and supported programming language are Java, a language that most big data developers are familiar with. Therefore, when it is necessary to modify the platform during the development, it is easier to modify JStorm than other platforms such as Storm, which is implemented with Clojure.  
\CARNET{} also employs Hadoop as the offline processing subsystem. Applications in IoV require not only real-time stream processing but also large-scale batch processing. However, such requirement is not the only reason why batch processing is used here. The data-quality issue is another reason. According to our empirical study of the data, nearly 10\% data are disordered or delayed, and less than 0.1\% data are substantially delayed, or even lost.  Although such data occupy a minor part of the whole dataset, they still affect the stream processing significantly in terms of accuracy and system performance. Consider multi-stream matching as an example. The basic requirement is matching stream A with stream B. In this task, if some data of stream B are delayed or lost, then the data in stream A may not find their matches. As a result, the program has to wait or at least buffer more data instances for stream A in case the delayed data in stream B might come later. This solution is common, but sometimes it can be risky because increasing the buffer length may cause memory issues. In our implementation, we detect the data-quality issue and split the bad part of the data for offline processing. Online stream processing deals with only the good part of the data. Offline processing runs with a given interval when most data are ready. When an application launches a request, both online and offline processed results are queried. This combination of online with offline is widely used in \CARNET{} to achieve real-time performance for most data, and \CARNET{}  tries its best to offer a better result than performing online processing only.

The data bus layer in \CARNET{} is implemented with KAFKA, one of the most powerful distributed messaging platforms. KAFKA can be easily integrated with most stream processing platforms and hence is widely used as industrial solutions. In fact, there are also many other similar platforms that can be used as the data bus, such as MetaQ, RocketMQ, ActiveMQ, and RabbitMQ. 

\subsection{Streaming Data Management Subsystem}

\CARNET{} needs to manage terabytes of driving data and further to provide services based on the data analytics. The stream is formed by a great number of data instances, each of which takes a hundred bytes. \CARNET{} collects nearly a billion of data instances every day. The value density of such data is very low, as few useful information can be extracted, due to the high sampling frequency. Moreover, the data value decreases fast with time because most of the applications, especially the time-critical applications, in IoV care more about the newly generated data. Though, the historical data cannot be simply compressed or discarded because the data might still be useful for certain applications.

We try to address the issue of data storage in IoV from two aspects. The first aspect is data separation. We identify those applications that have high real-time requirements so that we can further identify the corresponding hot data (such as the vehicle current status). We then separate the hot data with the cold data and try to put them into different storage platforms with different performance. The second aspect is preprocessing. Big dataset scan for the database is painful, but the data appending (write) can be fast. Therefore, we try to extract a small dataset with a higher density of value by using stream processing. We then separate this part of data with the raw data. In this manner, 
the storage subsystem can provide high throughput for data write, and high performance for the queries that are based on the small processed data. Furthermore, we put some hot data in in-memory cache to achieve a real-time data access.

To construct a data management subsystem, we adopted three different storage platforms in \CARNET{}, including in-memory caching, relational database, and NOSQL big data storage. There are multiple selections for each kind of storage platform. Many NOSQL databases, such as Cassandra, HBase, and MongoDB, can be adopted. In practice, we use HBase as the archive storage platform for the raw data. HBase can easily integrate with Hadoop to achieve big data batch processing ability. HBase provides a high-performance key-value storage, which perfectly fits the requirements of storing the raw vehicle data. The raw data include vehicle status, vehicle trajectories, driver activities related data, and user-order related data. Those data use ID (vehicle ID, user ID, driver ID) and timestamp as the index key. In the technical decision making procedure, we conducted experiments on managing raw driving data with RDBMS such as Mysql and Postgresql. In the test, we use $<ID, timestamp>$ as the index. The result shows that the relational databases have to maintain huge index files to index the big dataset. In another word, the data access performance of RDBMS does not scale with data volume. However, as a distributed key-value storage, HBase has the scalability with data volume.

Postgresql database is used to store relational data, and some of the processed data, to facilitate the queries from the web server. Compared with other relational databases such as MySQL, DB2, Sybase, the Postgresql has more comprehensive extended spatial functions. This feature is especially important because IoV applications have many location related processing requirements. Even though such requirements are mostly satisfied by the processing subsystem, the database still needs to provide several spatial related query abilities. Postgresql has a spatial database extension named PostGIS, which offers many spatial related functions that are rarely found in other competing spatial databases.  

In-memory caching plays a key role in accelerating the real-time applications in \CARNET{}. Memcached and Redis are popular in-memory caching platforms; they both provide high-performance key-value storage. Memcached provides extremely high performance on key-value based caching; therefore, the earlier version of \CARNET{} adopts Memcached to be the data buffering and high-performance data storage media. Later we found that Memcached has limitation in its functionalities and supported data types. As a comparison, Redis supports more data types and data accessing functionalities; Redis also provides data persistence in case of memory data lost. We then switched to Redis to seek for more support on multiple data types. A typical application example of using in-memory caching to improve performance is real-time vehicle tracking. In such application, the vehicle uploads data to the server, and the data are further processed by the stream processing subsystem; the processed results are updated to Redis for further use. Both the data producer (processing subsystem) and consumer (web server) can achieve high performance with this solution. 

\subsection{ Three-Layered Monitoring}

Monitoring how the system runs is an essential method to provide highly dependable services \cite{Fu}. By monitoring the system, maintainers can take corresponding actions timely when something goes wrong. \CARNET{} can be roughly separated into three layers, the infrastructure layer, which includes the cluster and the operating system; the computing platform layer, which includes the processing platforms such as Hadoop, Storm; and the application layer that is formed by the applications. To assure high dependability, each of the three layers needs to be monitored.

Typically, for infrastructure layer, developers often use cluster monitoring tools such as Nagios \cite{Josephsen}, Ganglia \cite{Ganglia}, or Zabbix \cite{Zabbix} to monitor the memory, network, I/O, and CPU load of the servers. Such monitoring tools also provide a useful dashboard to help to visualize the monitoring. The processing subsystem is formed by multiple processing platforms, which usually provide monitor tools to track how the platform works. It is also an option to use the third-party tools to monitor these platforms \cite{Dean}. The monitoring of infrastructure layer and computing platform layer provide useful information for assuring service dependability. Infrastructure monitoring has become one of the basic system deployment and maintenance requirements. In \CARNET{}, we monitor the infrastructure with Ganglia. Compared with other monitor tools, Ganglia provides powerful functions to monitor the cluster performance from a comprehensive view, it also enables users to define their own monitor items. Ganglia collects cluster information with very low cost. Through the dashboard provided by Ganglia, developers can obtain an overall view of the whole cluster. For computing platform monitoring, our monitoring subsystem simply integrates the monitoring APIs provided by each platform. 

\begin{figure}[t]
	\centering
	\includegraphics[width=1.0\linewidth]{picture/monitor.pdf}
	\vspace{-1em}
	\caption{A user interface illustration of the three-layered monitoring subsystem.}
	\vspace{-1em}
	\label{fig:monitor}
\end{figure}

However, our maintenance experience on \CARNET{} suggests that application monitoring is also essential. The applications in IoV run permanently with the streaming data continuously come. Meanwhile, the applications are always evolving with business changes. The evolving requires abundant information of the old versions of applications including how each application runs. For example, \emph{Is the parallelism enough for a specific application? How the application works on handling the burst stream spikes? Which task is the bottleneck in the processing topology (which task caused the congestion)?}  The answers to these questions are highly related to the monitoring of the application layer. From this perspective, the monitoring of application layer is as important as the monitoring of the infrastructure layer and the computing platform layer. Therefore, we empowered application layer monitoring into the monitoring subsystem of \CARNET{}. In practice, we embedded into each application a module to monitor the parameters that can reflect the performance and the health status of the applications; such  parameters include the buffer queue length, the streaming speed, heartbeat, etc. Those parameters are gathered into a shared in-memory database and further being analyzed in near real-time by a center monitor server.  Moreover, we employ anomaly detection algorithms on these collected parameters to predict the potential system failures. The monitoring server sends an alert message to maintainers when an abnormal is detected. 

With the help of this monitoring subsystem, the reaction time to system problems in \CARNET{} can be reduced from hours to seconds. Figure \ref{fig:monitor} illustrates the three-layered monitoring subsystem from a user interface perspective. The abundant application-running information that have been collected are further used in improving the alert accuracy of the monitoring. For example, we update the alerting thresholds to more proper ones based on the analysis of historical monitoring information; therefore, the spam alert messages, which are the pain point of maintainers, can be significantly reduced. The daily number of alert messages of \CARNET{} is reduced from tens to just a few routine notifications.  



\subsection{Performance Evaluation}

The performance of each platform used in \CARNET{} is evaluated by researchers and the platforms are proving to be high performance. Here, to answer the question that whether the integrated system still scales with data volume and have high performance, we conduct an end-to-end performance evaluation of the entire stream-processing procedure from the data receiving to the data query. The processing subsystem is deployed on a virtualized cluster with 25 servers, each server has 16GB memory, a 2-core 2.0 GHz CPU, 2*10 Gbps Emulex NIC. A dedicated server, which has 2*Intel XEON E5-2630v3, 64GB memory, 2*120G SSD and 102TB HDD, is allocated to deploy the database. The HBase and HDFS are deployed in a non-virtualized environment with three 12-node clusters. The total capacity of HDFS is above 600TB. We test the system from the following aspects.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.5\linewidth]{picture/delay.pdf}
	\vspace{-1em}
	\caption{A statistic of the delay in data processing.}
	\vspace{-1.5em}
	\label{fig:delay}
\end{figure}


\textbf{Throughput.} \CARNET{} can easily handle the data stream uploaded by 30,000 vehicles. We further simulate a high data workload by injecting the historical dataset back into the system with high frequency. With the same deployment, the total processing throughput of \CARNET{} has reached 240,000 data instances per second (each data instance is around 100KB). 

\textbf{End-to-end delay.} This test shows whether the system can satisfy the low-delay requirement of time-critical applications. We use real-time vehicle tracking as the test application. In this application, the data are uploaded from the vehicles and go through a series of platforms including the buffering platform, stream processing platform, in-memory caching platform, and then finally being queried by the web server. For the end-to-end delay, we compare the timestamps between the data being generated and the data being processed. The result, as illustrated in Figure \ref{fig:delay}, shows that the average delay is less than 2 seconds; such delay is acceptable in this scenario. We also monitor the length of the queue inside of the computing tasks; such queue length may also reflect the processing delay in an indirect way. The result is shown in Figure \ref{fig:queuelength}. It can be inferred from the evaluation result that the buffering subsystem performs well on buffering the workload of peak hours; the buffering slightly increases the processing delay, but the processing subsystem can catch up on dealing with the workload quickly. 

\begin{figure}[t]
	\centering
	\includegraphics[width=0.90\linewidth]{picture/queuelength.pdf}
	\vspace{-1em}
	\caption{The fluctuation of buffered queue length of stream processing subsystem.}
	\vspace{-1em}
	\label{fig:queuelength}
\end{figure}

\textbf{Processing scalability.} Scalability is a critical issue for \CARNET{} as the fleet scale increases over time. We test the processing scalability of \CARNET{} by increasing the deployment scale of the buffering and processing subsystems. In this test, we use the electronic fence as the foundation application. We deploy the processing subsystem on one server and increase the number of servers gradually. By injecting a large volume of data into the system, we evaluate the relationship between the throughput and the system scale. The evaluation result, as illustrated in Figure \ref{fig:scalability}, shows a near linear scalability of the processing subsystem. This performance can be attributed to two reasons, the distributed design of the processing platform, and the naturally distributed characteristic of the vehicle data. As the fleet scales up, we can always partition the data into small streams and deploy high-performance processing tasks accordingly.

\begin{figure}[t] 
	\centering
	\includegraphics[width=0.90\linewidth]{picture/scalability.pdf}
	\vspace{-1em}
	\caption{The scalability evaluation of \CARNET{}.}
	\vspace{-2em}
	\label{fig:scalability}
\end{figure}


\begin{figure*}[t]
	\centering
	\includegraphics[width=0.95\linewidth]{picture/systemoverview.pdf}
	\vspace{0em}
	\caption{An overview of \CARNET{}.}
	\vspace{-1em}
	\label{fig:modules}
\end{figure*}


\section{Overall Architecture}
\label{sec:architecture}

Based on the aforementioned challenges and the requirements, we design \CARNET{}, a high-performance big data analytics system for IoV. In this section, we present the overview of \CARNET{} and its comparison with other possible solutions. As illustrated in Figure \ref{fig:architecture}, \CARNET{} is an integrated system with multiple layers involved. The components that are marked by the dotted line are the core parts of \CARNET{} and therefore are discussed in detail in this paper.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.97\linewidth]{picture/architecture.pdf}
	\vspace{-0.8em}
	\caption{The system architecture of \CARNET{}.}
	\vspace{-1.5em}
	\label{fig:architecture}
\end{figure}

There can be multiple design choices for fulfilling IoV requirements. The traditional OLTP/OLAP solutions adopt a database-centric design. In such architecture, the uploaded driving data are normally stored in a relational database; the services are implemented based on the queries of the database. Such solution is simple, mature, and easy to deploy and maintain, qualifying such solution to be an option for IoV scenarios with a small-scale fleet. However, for large-scale scenarios, the database-centric architecture would suffer from the poor throughput of data reading and writing. Since the relational database is designed to fulfill the common requirements of transactional processing; it is hard to tailor the database platform in a fine-grained manner, dropping the unnecessary indexing and transactional protection, which consequently cause bad system performance.  

Nowadays, as applications related to streaming data become popular, many stream-processing architectures have been proposed. The architecture comprising distributed buffering, stream processing, and big data storage is widely adopted. Such solution can satisfy many IoV scenarios. However, it is not sufficient to deploy the stream processing alone. The comprehensive and evolving application requirements, as well as the varying data characteristics, require both online and offline processing ability. A good design to well connect different loosely coupled components is a key to the success of the system.



\subsection{Data Bus Layer}

The data bus layer provides a data-exchange hub for system modules to connect different computing tasks. This layer is implemented with a distributed messaging platform with fault-tolerant, high performance, and horizontally scalable ability. This layer also aims at providing a buffer for adjusting the stream volume of downstream data-processing procedures. By eliminating the time-varying spike of data streams, the downstream processing subsystems can take a routine pace to deal with the streams, without worrying about the system capacity being exceeded. 

\subsection{Processing Layer}

The processing layer includes two parts: the online stream-processing subsystem and the offline batch-processing subsystem. The stream processing subsystem is designed to provide a preprocessing functionality to the streams in a way that the query pressure of the database can be reduced. Without preprocessing, the applications based on the raw data would bring huge read pressure to the database. Since the large volume of raw data will be continuously stored, the writing burden cannot be reduced. The system would suffer from the read and write operations simultaneously if they both apply to a single target. An extracted subset of the raw data can be generated and maintained for further query by preprocessing. The result of preprocessing is incrementally updated in a data-driven manner. Such solution satisfies many IoV applications that depend more on the extracted data subset rather than the whole dataset. In this manner, the read and write pressure of the database can be separated, leading to a more friendly architecture to scale to a larger data volume and more applications.

The batch-processing subsystem is employed to satisfy the complex analysis requirements that involve more historical data. Moreover, considering that the low quality of the raw data may affect the accuracy of stream processing, the result of batch processing can also play a role as an auxiliary measurement to evaluate the quality of the stream-processing result.
Combining both the stream processing and batch processing, \CARNET{} can satisfy the needs of a wide range of workloads and use cases, in which low-latency reads and frequent updates of data are required. 

\subsection{Data Management Layer}

The data management layer adopts a heterogeneous structure constructed with multiple databases. In \CARNET{}, both NOSQL and SQL databases are adopted to achieve high performance. The design decision is based on a comprehensive analysis of the IoV applications. We store the data in different storage systems per the access requirements of the applications. 

More specifically, the most frequently accessed data are stored in the in-memory caching subsystem so that the data can be accessed and updated frequently. By using an in-memory key-value data structure, the in-memory caching subsystem can achieve a milliseconds latency and a high throughput in data updating and exchanging. Choices of in-memory caching can be multiple, such as Memcached \cite{Fitzpatrick} and Redis \cite{Carlson}. These platforms are originally designed for caching objects of a web server to reduce the pressure of the database. The in-memory caching employed here acts as the fast media of data update and exchange for applications. On one hand, the processing subsystem keeps updating the current result into the caching; on the other hand, the web servers keep fetching the processing results from the caching. An application example here is vehicle tracking, in which the stream processing subsystem updates the statuses of the vehicles, while the users check these statuses simultaneously through the web browser. 
Then, the structured and preprocessed data, which can be some smaller data streams than the volume of raw data, are stored in RDBMS and being accessed with SQL. By leveraging the mature object-relation mapping frameworks, it is much easier for RDBMS to be integrated to build web user interfaces.
Finally, all the data generated in \CARNET{} are archived in the distributed NOSQL big data storage subsystem, which supports only the non-realtime offline data processing. This architecture increases the system complexity, but it also accommodates the various requirements of different applications and can reduce the risk of data loss in a single-system failure.


\subsection{System Monitoring}

System monitoring is a unified name for a three-layered monitoring subsystem of \CARNET{}. We monitor the system from three different layers: infrastructure, computing platform, and application. It is necessary to monitor each of the layers. The infrastructure monitoring is used to view either live or archived statistics covering metrics such as the average CPU load and the network utilization of the cluster. The monitoring infrastructure provides not only a real-time health view of the hardware but also a guidance for adjusting the system deployment in terms of load balancing and resource utilization. For the computing platform layer, we monitor the platforms such as the stream-processing subsystem, the batch-processing subsystem, and the  databases to track the running status of the platform software. Specifically, the applications in \CARNET{} are monitored because the monitoring information collected for the applications can directly reflect the health status of the provided services. Monitoring the application layer can provide useful information for service-failure prevention or recovery.
